#include <cassert>
#include <cstdio>
#include <memory>
#include <string>

using std::string;
using std::shared_ptr;

class ImList
{
private:
    string head_;
    shared_ptr<ImList> tail_;

public:
    ImList(string head, shared_ptr<ImList> tail)
    : head_(head)
    , tail_(tail)
    {}

    string to_string()
    {
        return head_ + (tail_ == NULL ? "" : tail_->to_string());
    }
};

shared_ptr<ImList> added(shared_ptr<ImList> old, string item)
{
    return shared_ptr<ImList>(new ImList(item, old));
}

class FakeWorld
{
private:
    shared_ptr<ImList> stdout_;
public:
    FakeWorld()
    : stdout_(NULL)
    {}

    FakeWorld(shared_ptr<ImList> out)
    : stdout_(out)
    {}

    FakeWorld print(string msg)
    {
        return FakeWorld(added(stdout_, msg));
    }

    string all_stdout()
    {
        return stdout_ == NULL ? "" : stdout_->to_string();
    }
};


class RealWorld
{
public:
    RealWorld print(string msg)
    {
        printf("%s", msg.c_str());
        return *this;
    }
};

template<typename W>
W estimate(W world)
{
    world = world.print("3");
    world = world.print(".");
    world = world.print("1");
    return world;
}

int main()
{
    FakeWorld f;
    FakeWorld f2 = estimate(f);
    assert(f2.all_stdout() == "1.3");

    RealWorld r;
    estimate(r);
}
