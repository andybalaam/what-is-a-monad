
all: run-hs run-cpp run-testable-cpp

run-hs: pi-hs
	./$<
	@echo

pi-hs: pi.hs
	ghc -Wall -o $@ $<

run-cpp: pi-cpp
	./$<
	@echo

pi-cpp: pi.cpp
	g++ -Wall -o $@ $<

run-testable-cpp: testablepi-cpp
	./$<
	@echo

testablepi-cpp: testablepi.cpp
	g++ -Wall -o $@ $<

clean:
	rm pi-hs pi-cpp testablepi-cpp
